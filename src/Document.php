<?php

namespace DavidMaes\ElasticSearch;

use stdClass;

class Document
{

    /**
     * @var string
     */
    private $index;

    /**
     * @var string
     */
    private $type;

    /**
     * @var string
     */
    private $id;

    /**
     * @var stdClass
     */
    private $data;

    /**
     * Document constructor.
     *
     * @param string $index
     * @param string $type
     */
    public function __construct(string $index, string $type)
    {
        $this->index = $index;
        $this->type = $type;
        $this->data = new stdClass();
    }

    /**
     * Sets a property on this document.
     *
     * @param $name
     * @param $value
     */
    public function __set($name, $value)
    {
        $this->data->{$name} = $value;
    }

    /**
     * Gets a property on from document.
     *
     * @param $name
     * @return mixed
     */
    public function __get($name)
    {
        return $this->data->{$name};
    }

    /**
     * @return string
     */
    public function getIndex(): string
    {
        return $this->index;
    }

    /**
     * @param string $index
     */
    public function setIndex(string $index): void
    {
        $this->index = $index;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type): void
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return stdClass
     */
    public function getData(): stdClass
    {
        return $this->data;
    }

    /**
     * @param stdClass $data
     */
    public function setData(stdClass $data): void
    {
        $this->data = $data;
    }
}
