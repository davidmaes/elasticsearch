<?php

namespace DavidMaes\ElasticSearch;

use Elasticsearch\ClientBuilder;
use Elasticsearch\Common\Exceptions\Missing404Exception;

class ElasticSearch
{
    /**
     * @var ClientBuilder
     */
    private $client;

    /**
     * ElasticSearch constructor.
     *
     * @param array $hosts
     */
    public function __construct(array $hosts)
    {
        $builder = ClientBuilder::create();
        $builder->setHosts($hosts);
        $this->client = $builder->build();
    }

    /**
     * @param string $index
     * @param string $type
     * @param string $id
     * @return Document
     */
    public function get(string $index, string $type, string $id)
    {
        $params = [
            'index' => $index,
            'type' => $type,
            'id' => $id
        ];

        try {
            $response = $this->client->get($params);
        } catch (Missing404Exception $e) {
            return null;
        }

        $document = new Document(
            $response['_index'],
            $response['_type']
        );

        $document->setId($response['_id']);
        $source = $response['_source'];

        foreach ($source as $key => $value) {
            $document->{$key} = $value;
        }

        return $document;
    }

    /**
     * Saves a single document.
     *
     * @param Document $document
     */
    public function save(Document $document)
    {
        $params = [
            'index' => $document->getIndex(),
            'type' => $document->getType(),
            'body' => (array)$document->getData()
        ];

        $response = $this->client->index($params);
        $document->setId($response['_id']);
    }


    /**
     * @param string $index
     * @param string $type
     * @param string $id
     * @return void
     */
    public function delete(string $index, string $type, string $id)
    {
        $params = [
            'index' => $index,
            'type' => $type,
            'id' => $id
        ];

        $this->client->delete($params);
    }
}
