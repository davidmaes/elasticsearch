<?php

use DavidMaes\ElasticSearch\Document;
use DavidMaes\ElasticSearch\ElasticSearch;
use PHPUnit\Framework\TestCase;

class DocumentTest extends TestCase
{
    /**
     * Tests whether the configuration is loaded and correct.
     */
    public function test_set_get()
    {
        $elastic = new ElasticSearch(['elasticsearch:9200']);
        $document = new Document('test', 'doc');
        $document->random = rand(10,100);

        $elastic->save($document);

        $result = $elastic->get(
            $document->getIndex(),
            $document->getType(),
            $document->getId()
        );

        $this->assertEquals($document, $result);
    }

    /**
     * Tests whether the configuration is loaded and correct.
     */
    public function test_delete()
    {
        $elastic = new ElasticSearch(['elasticsearch:9200']);
        $document = new Document('test', 'doc');
        $document->random = rand(10,100);

        $elastic->save($document);

        $elastic->delete(
            $document->getIndex(),
            $document->getType(),
            $document->getId()
        );

        $result = $elastic->get(
            $document->getIndex(),
            $document->getType(),
            $document->getId()
        );

        $this->assertNull($result);
    }
}
