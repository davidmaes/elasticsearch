# Elasticsearch
This library is puts a lightweight shell around ElasticSearch for ease of use.

## Getting Started
### Prerequisites
You will need docker and docker-compose to run the tests in this library.

### Installation
Just run the included composer when you first install it.
```
docker-compose run php composer install
```
## Running the tests
```
docker-compose run php vendor/bin/phpunit
```
## Deployment
To add this library to your application, install it with Composer
```
composer require davidmaes/elasticsearch

```